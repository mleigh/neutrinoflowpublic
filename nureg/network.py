"""
A collection of networks for the neutrino regression package
"""

import torch as T

from nflows import flows, distributions

from mattstools.network import MyNetBase
from mattstools.torch_utils import get_loss_fn
from mattstools.modules import DeepSet, DenseNetwork

from nureg.transforms import stacked_norm_flow


class SingleNeutrinoNSF(MyNetBase):
    """A conditional autoregressive neural spline flow for single neutrino regression
    - As this is an INN there is some variable name changes that need to be claified
        - The 'input' is usually the observed information, so here it becomes
          the contextual information
        - The 'output' or 'target' is the truth level information so here it becomes
          the features of the flow
    """

    def __init__(
        self,
        base_kwargs: dict,
        flow_kwargs: dict,
        dpset_kwargs: dict,
        embd_kwargs: dict,
    ) -> None:
        """
        base_kwargs will contain the input dimension expressed as a list for the
        [MET, leptons, misc, jets] attributes

        args:
            dpset_kwargs: A dictionary containing the keyword arguments for the deep set
            flow_kwargs: A dictionary containing the keyword arguments for the flow
            embd_kwags: Keyword arguments for the context embedding network
        """
        super().__init__(**base_kwargs)

        ## Initialise the deep set
        self.jet_ds = DeepSet(
            self.inpt_dim[-1],
            ctxt_dim=sum(self.inpt_dim[0:3]),  ## met, lepton, and misc inputs
            **dpset_kwargs
        )

        ## Initialise the context embedding network
        self.embd_net = DenseNetwork(
            inpt_dim=sum(self.inpt_dim[0:3]) + self.jet_ds.outp_dim,
            **embd_kwargs
        )

        ## Save the flow: a combination of the inn and a gaussian
        inn = stacked_norm_flow(
            self.outp_dim, ctxt_dim=self.embd_net.outp_dim, **flow_kwargs
        )
        self.flow = flows.Flow(inn, distributions.StandardNormal([self.outp_dim]))

        ## Move the network to the selected device
        self.to(self.device)

    def get_flow_tensors(self, sample):
        """Given a sample (including the target), get the contextual and target tensors
        for the the normalising flow
        - called for forward passes
        - includes passing the jets through the deep set
        """

        ## Unpack the sample
        met, lept, misc, jets, neut = sample

        ## Pass the data through a conditional deep set
        jets = self.jet_ds.forward(
            tensor=jets,
            mask=jets[..., 0] != -T.inf,
            ctxt=T.cat([met, lept, misc], dim=-1),
        )
        ctxt = T.cat([met, lept, misc, jets], dim=-1)

        ## Pass through the embedding network
        ctxt = self.embd_net(ctxt)

        ## Return the context AND the target
        return ctxt, neut

    def get_losses(self, sample) -> dict:
        """For this model there is only one loss and that is the log probability"""
        ctxt, neut = self.get_flow_tensors(sample)
        loss_dict = self.loss_dict_reset()
        loss_dict["total"] = -self.flow.log_prob(neut, context=ctxt).mean()
        return loss_dict

    def generate(self, sample, n_points=1) -> T.Tensor:
        """Generate points in the X space by sampling from the latent"""
        ctxt, _ = self.get_flow_tensors(sample)
        generated = self.flow.sample(n_points, context=ctxt).squeeze()
        return generated

    def sample_and_log_prob(self, sample, n_points=100) -> T.Tensor:
        """Generate many points per sample and return all with their log likelihoods"""
        ctxt, _ = self.get_flow_tensors(sample)
        return self.flow.sample_and_log_prob(n_points, ctxt)

    def get_mode(self, sample, n_points=100) -> T.Tensor:
        """Generate points, then select the one with the most likely value in the
        reconstruction space
        """
        gen, log_like = self.sample_and_log_prob(sample, n_points=n_points)
        return gen[T.arange(len(gen)), log_like.argmax(dim=-1)]

    def forward(self, sample):
        """Get the latent space embeddings given neutrino and context information"""
        ctxt, neut = self.get_flow_tensors(sample)
        return self.flow.transform_to_noise(neut, context=ctxt)


class SingleNeutrinoFF(MyNetBase):
    """A simple Feed forward network made up of a DeepSet+MLP to compare to the cINN"""

    def __init__(
        self,
        base_kwargs: dict,
        loss_nm: str,
        dpset_kwargs: dict,
        dns_kwargs: dict,
    ) -> None:
        """
        base_kwargs will contain the input dimension expressed as a list for the
        [MET, leptons, misc, jets] attributes

        args:
            loss_nm: Name of the loss function to use for neutrino regression
            dpset_kwargs: Keyword arguments for the deep set
            dns_kwargs: Keyword arguments for the dense network
        """
        super().__init__(**base_kwargs)

        ## Save the loss
        self.loss_fn = get_loss_fn(loss_nm)

        ## Initialise the deep set
        self.jet_ds = DeepSet(
            self.inpt_dim[-1],
            ctxt_dim=sum(self.inpt_dim[0:3]),  ## met, lepton, and misc inputs
            **dpset_kwargs
        )

        ## Save the dense network
        self.dns = DenseNetwork(
            inpt_dim=sum(self.inpt_dim[0:3]) + self.jet_ds.outp_dim,
            outp_dim=self.outp_dim,
            **dns_kwargs
        )

        ## Move the network to the selected device
        self.to(self.device)

    def forward(self, inputs):
        """Pass a sample through the network
        - called for forward passes
        - includes passing the jets through the deep set
        """

        ## Unpack the sample of inputs
        met, lept, misc, jets = inputs

        ## Pass the data through a conditional deep set
        jets = self.jet_ds.forward(
            tensor=jets,
            mask=jets[..., 0] != -T.inf,
            ctxt=T.cat([met, lept, misc], dim=-1),
        )
        inpt = T.cat([met, lept, misc, jets], dim=-1)

        ## Pass through the mlp
        return self.dns(inpt)

    def get_losses(self, sample) -> dict:
        """Calculate the regression loss"""
        loss_dict = self.loss_dict_reset()

        met, lept, misc, jets, neut = sample
        output = self.forward((met, lept, misc, jets))
        loss_dict["total"] = self.loss_fn(neut, output).mean()
        return loss_dict
