"""
Miscellaneous utility functions for the nureg package
"""

from typing import Tuple
import argparse

from copy import deepcopy

from mattstools.utils import key_change, args_into_conf, get_standard_configs, str2bool
from mattstools.torch_utils import get_act


def get_nureg_configs(def_netw="config/flow.yaml") -> Tuple[dict, dict, dict]:
    """Loads, modifies, and returns three configuration dictionaries using command
    line arguments for a graph discriminator
    """

    ## Use the common method to get the configs with usual decorations
    data_conf, net_conf, train_conf = get_standard_configs(def_netw=def_netw)

    ## Add in extra arguments for the regressor
    parser = argparse.ArgumentParser()

    ## Data preparation
    parser.add_argument(
        "--scaler_nm",
        type=str,
        help="Name of preprocessing scaler",
    )
    parser.add_argument(
        "--lep_vars",
        type=str,
        help="Name of the lepton and MET input coordinates seperated by commas",
    )
    parser.add_argument(
        "--jet_vars",
        type=str,
        help="Name of the jets input coordinates seperated by commas",
    )
    parser.add_argument(
        "--out_vars",
        type=str,
        help="Name of the output/target coordinates seperated by commas",
    )
    parser.add_argument(
        "--jet_pairs_in_misc",
        type=str2bool,
        help="If hand selected jet pair kinematics are included in the misc inputs",
    )

    ## Network flow setup
    parser.add_argument(
        "--nstacks", type=int, help="The number of stacked transforms in the flow"
    )
    parser.add_argument(
        "--param_func", type=str, help="Parameter learning function (made or cplng)"
    )
    parser.add_argument(
        "--invrt_func", type=str, help="Invertible function type (rqs or aff)"
    )
    parser.add_argument("--flow_nrm", type=str, help="The normalisation used in flow")

    ## Network and deep set setup
    parser.add_argument(
        "--do_ds", type=str2bool, help="If we should use the jets through a deep set"
    )
    parser.add_argument(
        "--pool_type", type=str, help="The pooling function applied in the deep set"
    )
    parser.add_argument(
        "--drp", type=float, help="Dropout probability for the hidden layers"
    )
    parser.add_argument(
        "--net_nrm", type=str, help="Normalisation layers to use in the networks"
    )

    ## Load the arguments
    args, _ = parser.parse_known_args()

    ## Some arguments are identified by the exact keys in each dict
    args_into_conf(args, data_conf, "scaler_nm")
    args_into_conf(args, data_conf, "lep_vars")
    args_into_conf(args, data_conf, "jet_vars")
    args_into_conf(args, data_conf, "out_vars")
    args_into_conf(args, data_conf, "jet_pairs_in_misc")
    args_into_conf(args, net_conf, "do_ds")

    ## Other arguments need more manual placement in the configuration dicts
    args_into_conf(args, net_conf, "nstacks", "flow_kwargs/nstacks")
    args_into_conf(args, net_conf, "param_func", "flow_kwargs/param_func")
    args_into_conf(args, net_conf, "invrt_func", "flow_kwargs/invrt_func")
    args_into_conf(args, net_conf, "flow_nrm", "flow_kwargs/nrm")
    args_into_conf(args, net_conf, "pool_type", "dpset_kwargs/pool_type")
    args_into_conf(args, net_conf, "pool_type", "dpset_kwargs/pool_type")
    args_into_conf(
        args,
        net_conf,
        "drp",
        [
            "dpset_kwargs/feat_net_kwargs/drp",
            "dpset_kwargs/attn_net_kwargs/drp",
            "dpset_kwargs/post_net_kwargs/drp",
            "flow_kwargs/net_kwargs/drp",
        ],
    )
    args_into_conf(
        args,
        net_conf,
        "net_nrm",
        [
            "dpset_kwargs/feat_net_kwargs/nrm",
            "dpset_kwargs/attn_net_kwargs/nrm",
            "dpset_kwargs/post_net_kwargs/nrm",
            "flow_kwargs/net_kwargs/nrm",
        ],
    )

    return data_conf, net_conf, train_conf


def change_kwargs_for_made(old_kwargs: dict)->Tuple[dict, int]:
    """Converts a dictionary of keyword arguments for configuring a mattstools
    DenseNetwork to one that can initialise a MADE network for the nflows package
    with similar (not exactly same) hyperparameters
    """
    new_kwargs = deepcopy(old_kwargs)

    ## Certain keys must be changed
    key_change(new_kwargs, "ctxt_dim", "context_features")
    key_change(new_kwargs, "drp", "dropout_probability")
    key_change(new_kwargs, "do_res", "use_residual_blocks")

    ## Certain keys are changed and their values modified
    if "act_h" in new_kwargs:
        new_kwargs["activation"] = get_act(new_kwargs.pop("act_h"))
    if "nrm" in new_kwargs:  ## MADE only has batch norm!
        new_kwargs["use_batch_norm"] = new_kwargs.pop("nrm") is not None

    ## Some options are missing
    missing = ["ctxt_in_all", "n_lyr_pbk", "act_o", "do_out"]
    for miss in missing:
        if miss in new_kwargs:
            del new_kwargs[miss]

    ## The hidden dimension must be passed to MADE as an arg, not a kwarg
    if "hddn_dim" in new_kwargs:
        hddn_dim = new_kwargs.pop("hddn_dim")
    else:
        ## Use the same default value for mattstools.modules.DenseNet
        hddn_dim = 32

    return new_kwargs, hddn_dim
