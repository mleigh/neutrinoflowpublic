"""
Main callable script to train the neutrino regressor
"""
from pathlib import Path
from joblib import load
from mattstools.trainer import Trainer
from mattstools.utils import save_yaml_files

import torch as T

from nureg.network import SingleNeutrinoNSF, SingleNeutrinoFF
from nureg.datasets import NuRegData
from nureg.utils import get_nureg_configs

## Manual seed for reproducibility
T.manual_seed(42)


def main():
    """Run the script"""

    ## Collect the session arguments, returning the configuration dictionaries
    data_conf, net_conf, train_conf = get_nureg_configs()

    ## Load the ttbar regression data and apply normalisation
    train_set = NuRegData(dset="test", **data_conf)

    ## Get the data dimensions and add them the network configuraiton
    all_dims = train_set.get_dim()
    net_conf["base_kwargs"]["outp_dim"] = all_dims.pop(-1)  ## Neutrino dim is last
    net_conf["base_kwargs"]["inpt_dim"] = all_dims

    ## Load the network
    if "flow_kwargs" in net_conf:
        network = SingleNeutrinoNSF(**net_conf)
    else:
        network = SingleNeutrinoFF(**net_conf)
    print(network)

    ## Load and apply the saved scalers if resuming
    if train_conf["resume"]:
        scalers = load(Path(network.full_name, "scalers"))
        train_set.apply_preprocess(scalers)

    ## Calculate new scalers and make plots before and after processing
    else:
        train_set.plot_variables(Path(network.full_name, "train_dist"))
        scalers = train_set.save_preprocess_info(Path(network.full_name, "scalers"))
        train_set.apply_preprocess(scalers)
        train_set.plot_variables(Path(network.full_name, "train_dist"))

    ## Load the trainer
    trainer = Trainer(network, train_set, **train_conf)

    ## Create the save folder for the network and store the configuration dicts
    save_yaml_files(
        Path(network.full_name, "config"),
        ["data", "netw", "train"],
        [data_conf, net_conf, train_conf],
    )

    ## Run the training looop
    trainer.run_training_loop()


if __name__ == "__main__":
    main()
